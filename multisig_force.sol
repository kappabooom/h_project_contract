pragma solidity ^0.4.25;

contract multiSig {
    address internal assetsOwner;//用戶
    address internal platform;
    address internal thirdparty;
    uint private owner_flag;
    uint private platform_flag;
    uint private thirdparty_flag;

    event changeAssetOwnerEvent(address indexed oldOwner, address indexed newOwner, uint256 timestamp);
    event changePlatformEvent(address indexed oldPlatform, address indexed newPlatform, uint256 timestamp);
    event changeThirdpartyEvent(address indexed oldThirdparty, address indexed newThirdparty, uint256 timestamp);


    //執行更換eth地址前 三人中有兩人須簽章
    function ownerSign() public isOwner {
        owner_flag = 1;
    }

    function platformSign() public isPlatform {
        platform_flag = 1;
    }

    function thirdpartySign() public isThirdparty {
        thirdparty_flag = 1;
    }

    modifier isMultiSignature(){
        require(owner_flag+platform_flag+thirdparty_flag >= 2);
        _;
    }

    //重置簽章狀態
    function resetSignStatus() internal {
        owner_flag = 0;
        platform_flag = 0;
        thirdparty_flag = 0;
    }

    //檢查是否為合約擁有者
    modifier isOwner(){
        require(msg.sender == assetsOwner);
        _;
    }

    //檢查是否為平台方
    modifier isPlatform(){
        require(msg.sender == platform);
        _;
    }

    //檢查是否為第三方公信機構
    modifier isThirdparty(){
        require(msg.sender == thirdparty);
        _;
    }

    //更換assetOwner
    function changeAssetOwner(address _to) public isMultiSignature{
        assetsOwner = _to;
        resetSignStatus();

        emit changeAssetOwnerEvent(msg.sender, _to, now);
    }

    //更換platform
    function changePlatform(address _to) public isMultiSignature{
        platform = _to;
        resetSignStatus();

        emit changePlatformEvent(msg.sender, _to, now);
    }

    //更換thirdparty
    function changeThirdparty(address _to) public isMultiSignature{
        thirdparty = _to;
        resetSignStatus();

        emit changeThirdpartyEvent(msg.sender, _to, now);
    }

    function getOwnerSign() public view returns(uint){
        return owner_flag;
    }

    function getPlatformSign() public view returns(uint){
        return platform_flag;
    }

    function getThirdpartySign() public view returns(uint){
        return thirdparty_flag;
    }

    function getAssetsOwner() public view returns(address){
        return assetsOwner;
    }

    function getPlatform() public view returns(address){
        return platform;
    }

    function getThirdparty() public view returns(address){
        return thirdparty;
    }

}